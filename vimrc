set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'pangloss/vim-javascript', { 'for': 'javascript' }
Plugin 'mxw/vim-jsx'
Plugin 'elzr/vim-json'
Plugin 'Raimondi/delimitMate'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'Lokaltog/powerline', {'rtp': 'powerline/bindings/vim/'}
Plugin 'jiangmiao/auto-pairs'
Plugin 'jnurmine/Zenburn'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" general stuff
"Powerline

syntax enable           " enable syntax processing
filetype plugin indent on    " enables filetype detection
set noswapfile        " I don't like swap files
set nu         " turn on numbering
" Use UNIX (\n) line endings.
au BufNewFile *.* set fileformat=unix

" Set the default file encoding to UTF-8:
set encoding=utf-8

" POWERLINE
" show powerline even if only one window is used
set laststatus=2 
" Powerline fonts
set guifont=Inconsolata\ for\ Powerline:h15
set t_Co=256
set fillchars+=stl:\ ,stlnc:\
set term=xterm-256color
set termencoding=utf-8

" COLOR
set t_Co=256
colors zenburn

" SHORTCUTS
nnoremap <F3> :set hlsearch!<CR>
nnoremap <F2> :set number!<CR>
nnoremap te :tabedit<Space>
" cd to dir of currently open file
command CC cd %:p:h

" SPLIT
" more natural split behavior
set splitbelow
set splitright
" easyer split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Number of spaces that a pre-existing tab is equal to.
au BufRead,BufNewFile *py,*.php set tabstop=4
"spaces for indents
au BufRead,BufNewFile *.py,*.php set shiftwidth=4
au BufRead,BufNewFile *.py,*.php set expandtab
au BufRead,BufNewFile *.py,*.php set softtabstop=4
"JavaScript and yaml
" Number of spaces that a pre-existing tab is equal to.
au BufRead,BufNewFile *js,*.jsx,*.json,*.yml,*.yaml set tabstop=2
"spaces for indents
au BufRead,BufNewFile *.js,*jsx,*.json,*.yml,*.yaml set shiftwidth=2
au BufRead,BufNewFile *.js,*jsx,*.json,*.yml,*.yaml set expandtab
au BufRead,BufNewFile *.js,*jsx,*.json,*.yml,*.yaml set softtabstop=2

" Use the below highlight group when displaying bad whitespace is desired.
highlight BadWhitespace ctermbg=red guibg=red

" Display tabs at the beginning of a line in Python mode as bad.
au BufRead,BufNewFile *.py,*.php,*.js,*.jsx,*.json,*.yml,*.yaml match BadWhitespace /^\t\+/
" Make trailing whitespace be flagged as bad.
au BufRead,BufNewFile *.py,*.php,*.js,*.jsx,*.json,*.yml,*.yaml match BadWhitespace /\s\+$/

let NERDTreeIgnore=['\.pyc', '\~$', '\.swo$', '\.swp$', '\.git', '\.hg', '\.svn', '\.bzr']
let NERDTreeShowHidden=1
let NERDTreeKeepTreeInNewTab=1
let g:nerdtree_tabs_open_on_console_startup=1
map <Leader>n <plug>NERDTreeTabsToggle<CR>
"locate current file in NERDTree
map <leader>l :NERDTreeFind<cr>

